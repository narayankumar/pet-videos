
build2014091802
- h4 title words thin and bold

build2014091801
- made title 'pet videos' in teasers view
- gave it class=back, h4, second word strong

build2014091501
- teasers view to have 'share | comment' instead of 'add comment' 
- comment form visible, not ajax

7.x-1.0-dev1 - initial commit on 12 sep 2014
