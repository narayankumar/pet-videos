<?php
/**
 * @file
 * pet_videos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pet_videos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function pet_videos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function pet_videos_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_pet_videos
  $nodequeues['ad_block_pet_videos'] = array(
    'name' => 'ad_block_pet_videos',
    'title' => 'Ad block - Pet Videos',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function pet_videos_flag_default_flags() {
  $flags = array();
  // Exported flag: "Content report abuse".
  $flags['content_report_abuse'] = array(
    'entity_type' => 'node',
    'title' => 'Content report abuse',
    'global' => 0,
    'types' => array(
      0 => 'forum',
      1 => 'pet_video',
      2 => 'node_gallery_item',
      3 => 'node_gallery_gallery',
    ),
    'flag_short' => 'Report abuse',
    'flag_long' => '',
    'flag_message' => 'Thanks for reporting. A site admin will look into it.',
    'unflag_short' => 'Cancel \'report abuse\'',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to report this content as abuse or being inappropriate?',
    'unflag_confirmation' => 'Are you sure want to cancel \'report abuse\'?',
    'module' => 'pet_videos',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function pet_videos_node_info() {
  $items = array(
    'pet_video' => array(
      'name' => t('Pet video'),
      'base' => 'node_content',
      'description' => t('You can share a pet video on YouTube with other Gingertail users'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Use the Title below to briefly explain the content of your video'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
