<?php
/**
 * @file
 * pet_videos.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pet_videos_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create pet_video content'.
  $permissions['create pet_video content'] = array(
    'name' => 'create pet_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any pet_video content'.
  $permissions['delete any pet_video content'] = array(
    'name' => 'delete any pet_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own pet_video content'.
  $permissions['delete own pet_video content'] = array(
    'name' => 'delete own pet_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any pet_video content'.
  $permissions['edit any pet_video content'] = array(
    'name' => 'edit any pet_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own pet_video content'.
  $permissions['edit own pet_video content'] = array(
    'name' => 'edit own pet_video content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag content_report_abuse'.
  $permissions['flag content_report_abuse'] = array(
    'name' => 'flag content_report_abuse',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'publish button publish any pet_video'.
  $permissions['publish button publish any pet_video'] = array(
    'name' => 'publish button publish any pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable pet_video'.
  $permissions['publish button publish editable pet_video'] = array(
    'name' => 'publish button publish editable pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own pet_video'.
  $permissions['publish button publish own pet_video'] = array(
    'name' => 'publish button publish own pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any pet_video'.
  $permissions['publish button unpublish any pet_video'] = array(
    'name' => 'publish button unpublish any pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable pet_video'.
  $permissions['publish button unpublish editable pet_video'] = array(
    'name' => 'publish button unpublish editable pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own pet_video'.
  $permissions['publish button unpublish own pet_video'] = array(
    'name' => 'publish button unpublish own pet_video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'rate content'.
  $permissions['rate content'] = array(
    'name' => 'rate content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'fivestar',
  );

  // Exported permission: 'unflag content_report_abuse'.
  $permissions['unflag content_report_abuse'] = array(
    'name' => 'unflag content_report_abuse',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
