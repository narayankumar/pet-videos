<?php
/**
 * @file
 * pet_videos.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function pet_videos_default_rules_configuration() {
  $items = array();
  $items['rules_report_content_abuse_to_site_admin'] = entity_import('rules_config', '{ "rules_report_content_abuse_to_site_admin" : {
      "LABEL" : "Report content abuse to site admin",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_content_report_abuse" : [] },
      "DO" : [
        { "mail" : {
            "to" : "webmaster@gingertail.in",
            "subject" : "[flagging-user:name] has reported content abuse",
            "message" : "hi:\\r\\n\\r\\nContent abuse reported by: [flagging-user:name]\\r\\nE-mail of abuse reporter: [flagging-user:mail]\\r\\n\\r\\n- - - - -\\r\\n\\r\\nTitle of content reported: [flagged-node:title]\\r\\n\\r\\nType of content reported: [flagged-node:content-type]\\r\\n\\r\\nDate of content created: [flagged-node:created]\\r\\n\\r\\nAuthor of content reported: [flagged-node:author]\\r\\n\\r\\nE-mail of Author: [flagged-node:author:mail]\\r\\n\\r\\nLink to content reported: [flagged-node:url]\\r\\n\\r\\n- - - - -\\r\\n\\r\\nYou can visit the link above to take corrective action, if any.\\r\\n\\r\\n-- Your friendly robot at gingertail.in\\r\\n",
            "from" : "[flagging-user:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
